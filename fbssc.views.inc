<?php

/**
 * @file
 * Provide views data and handlers for the Facebook-style Statuses module.
 */

/**
 * Implementation of hook_views_data().
 */
function fbssc_views_data() {
  //Basic table information.
  /*
  $data['fbssc']['table']['group']  = t('Facebook-style Statuses');
  $data['users']['table']['join'] = array(
    'fbssc' => array(
      'left_field' => 'uid',
      'field' => 'uid',
    ),
  );
  $data['fbssc']['table']['join'] = array(
    'users' => array(
      'left_field' => 'uid',
      'field' => 'uid',
      'type' => 'INNER',
    ),
  );

  //Advertise this table as a possible base table.
  $data['fbssc']['table']['base'] = array(
    'field' => 'uid',
    'title' => t('Facebook-style Statuses Updates'),
    'help' => t('Stores status updates.'),
    'weight' => 10,
  );
   */

  //Alias for Status ID to extend its use.
  $data['facebook_status']['comments'] = array(
    'title' => t('Status comments'),
    'help' => t('Shows the list of comments for the relevant status.'),
    'field' => array(
      'field' => 'sid',
      'handler' => 'fbssc_views_handler_field_comments',
      'click sortable' => FALSE,
    ),
  );
  $data['facebook_status']['comment-box'] = array(
    'title' => t('Status comment box'),
    'help' => t('Shows the form to submit a new status comment.'),
    'field' => array(
      'field' => 'sid',
      'handler' => 'fbssc_views_handler_field_comment_box',
      'click sortable' => FALSE,
    ),
  );

  return $data;
}

/**
 * Implementation of hook_views_data_alter().
 *
function fbssc_views_data_alter(&$data) {
  $data['fbssc']['users'] = array(
    'group' => t('Facebook-style Statuses'),
    'relationship' => array(
      'title' => t('Users'),
      'label' => t('Users'),
      'help' => t('Add a relationship to gain access to information related to the users who submitted the relevant statuses.'),
      'relationship table' => 'users',
      'relationship field' => 'uid',
      'base' => 'fbssc',
      'field' => 'uid',
      'type' => 'INNER',
      'handler' => 'views_handler_relationship',
    ),
  );
}
 */

/**
 * Implementation of hook_views_handlers().
 */
function fbssc_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'fbssc'),
    ),
    'handlers' => array(
      'fbssc_views_handler_field_comments' => array(
        'parent' => 'views_handler_field',
      ),
      'fbssc_views_handler_field_comment_box' => array(
        'parent' => 'views_handler_field',
      ),
    ),
  );
}