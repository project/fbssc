<?php

/**
 * @file
 *   Formats status comments in Views.
 */

/**
 * Field handler to list status comments.
 */
class fbssc_views_handler_field_comments extends views_handler_field {
  function render($values) {
    return theme('fbssc_items', fbssc_get_comments($values->facebook_status_sid));
  }
}